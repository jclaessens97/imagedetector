# ImageDetector

## Roadmap

- Version 1:
    - It needs to be possible to detect if target image contains source image
    - Simple GUI to do that ^
- Version 2:
    - It needs to be possible to detect if a youtube video contains source image
    - Simple GUI to do that ^

## How to run (docker)

You can run the project by using docker compose:
```docker-compose up -d```. The first time it will build both projects automatically, if you want to build again after that, you'll need to run ```docker-compose up --build``` or ```docker-compose build```.

## How to run (locally)

If you want to run it locally without the use of docker (for development), you need to add ```opencv4nodejs``` in the package.json of the backend project.
You then can run ```npm install``` on both backend and frontend project. ```opencv4nodejs``` is left out of package.json because the base docker image already has opencv installed globally.

To start the frontend you need ```npm run serve```.
To start the backend you need ```npm run dev```.

## Common TODOs (apart from roadmap)
- [ ] Tests