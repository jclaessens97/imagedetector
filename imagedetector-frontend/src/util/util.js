export function parseUrl(urlOrVideoId) {
  const regex = /(?:[?&]v=|\/embed\/|\/1\/|\/v\/|https:\/\/(?:www\.)?youtu\.be\/)([^&\n?#]+)/;
  const match = urlOrVideoId.match(regex);

  if (match && match[1].length === 11) {
    return match[1];
  }

  if (urlOrVideoId.length === 11) {
    return urlOrVideoId;
  }

  return '';
}
