import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api';

const uploadHeaders = {
  headers: { 'Content-Type': 'multipart/form-data;' },
};

export async function uploadImage(formData) {
  const response = await axios.post('/detect/image', formData, uploadHeaders);
  return response.data;
}

export async function uploadVideo(formData) {
  const response = await axios.post('/detect/video', formData, uploadHeaders);
  return response.data;
}
