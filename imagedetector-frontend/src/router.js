import Vue from 'vue';
import Router from 'vue-router';
import ImageCompare from './components/ImageCompare.vue';
import VideoCompare from './components/VideoCompare.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/imagedetector',
    },
    {
      path: '/imagedetector',
      name: 'image-compare',
      component: ImageCompare,
    },
    {
      path: '/videodetector',
      name: 'video-compare',
      component: VideoCompare,
    },
  ],
});
