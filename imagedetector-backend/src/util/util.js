import cv from 'opencv4nodejs';
import fs from 'fs';

const DEBUGGING = false;

// https://www.pyimagesearch.com/2015/01/26/multi-scale-template-matching-using-python-opencv/
export async function imageCompare(sourceFile, targetFile) {
  const minMatchTreshold = 0.85;

  // Load images
  const sourceMat = await cv.imreadAsync(`${sourceFile.path}`);
  const targetMat = await cv.imreadAsync(`${targetFile.path}`);

  // TODO: use edges instead of image for template matching?
  // TODO: optimize (very badly optimized atm)

  const scalesVector = linspace(0.2, 1.0, 20).reverse();
  for (let i = 0; i < scalesVector.length; i++) {
    const scale = scalesVector[i];
    const resizedSource = sourceMat.rescale(scale);

    if (targetMat.rows < resizedSource.rows || targetMat.cols < resizedSource.cols) {
      //when target image is smaller then souce image, break out of loop to prevent error.
      console.log('Target image is smaller then source image. Breaking out of loop...');
      break;
    }

    // Match images
    const matched = targetMat.matchTemplate(resizedSource, 5); // method 5 = CV_TM_CCOEFF_NORMED

    // Determine the 'match value' of the two images.
    // The method that is used (CV_TM_CCOEFF_NORMED),
    // will return a maxVal between 0 and 1, where 1 is a perfect match.
    const minMaxLoc = matched.minMaxLoc();

    if (DEBUGGING) {
      showDebugFrame(resizedSource, targetMat, minMaxLoc);
    }

    if (minMaxLoc.maxVal > minMatchTreshold) {
      return Promise.resolve(true); // match
    }
  }

  return Promise.resolve(false); // no match
}

export function remove(file) {
  if (typeof file === 'string') {
    fs.unlinkSync(file);
  } else {
    fs.unlinkSync(file.path);
  }
}

function showDebugFrame(sourceMat, targetMat, minMaxLoc) {
  const { maxLoc: { x, y } } = minMaxLoc;

  //Draw rectangle on image & show image
  targetMat.drawRectangle(
    new cv.Rect(x, y, sourceMat.cols, sourceMat.rows),
    new cv.Vec(0, 255, 0),
    2,
    cv.LINE_8
  );

  cv.imshow('Matching images', targetMat);
  cv.waitKey();
}

/**
 * Implementation of Matlab's linspace function.
 * Creates a vector of {numberOfChunks} evenly spaced points between {start} and {end}
 * @param {float} start
 * @param {float} end
 * @param {number} numberOfChunks
 * @returns {array}
 */
function linspace(start, stop, numberOfChunks) {
  if(typeof numberOfChunks === "undefined") numberOfChunks = Math.max(Math.round(stop-start)+1,1);
  if(numberOfChunks<2) { return numberOfChunks===1?[start]:[]; }
  let i,ret = Array(numberOfChunks);
  numberOfChunks--;
  for(i=numberOfChunks;i>=0;i--) { ret[i] = (i*stop+(numberOfChunks-i)*start)/numberOfChunks; }
  return ret;;
}
