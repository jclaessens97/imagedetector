import { Router } from 'express';
import multer from 'multer';
import * as imageController from '../../controllers/ImageController';
import * as videoController from '../../controllers/VideoController';

const router = Router();
const upload = multer({ dest: 'tmp/' });

router.post('/detect/image', upload.fields([
  {
    name: 'sourceImage',
    maxCount: 1,
  },
  {
    name: 'targetImage',
    maxCount: 1,
  },
]),
imageController.upload);

router.post('/detect/video', upload.single('sourceImage'), videoController.upload);

export default router;
