import * as imageService from '../services/ImageService';

export async function upload(req, res) {
  console.log('ImageController.upload() called');

  try {
    const result = await imageService.upload(req.files.sourceImage[0], req.files.targetImage[0]);
    res.status(200).json({ result });
  } catch (err) {
    // TODO: handle error correctly
    console.log(err);
    res.status(400).json({ errMessage: err });
  }
}
