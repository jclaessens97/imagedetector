import * as videoService from '../services/VideoService';

export async function upload(req, res) {
  console.log('VideoController.upload() called');

  try {
    const result = await videoService.upload(req.file, req.body.videoId, req.body.frequency);
    res.status(200).json({ result });
  } catch (err) {
    console.log(err);
    res.status(400).json({ errMessage: err });
  }
}
