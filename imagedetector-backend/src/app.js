import express from 'express';
import bodyParser from 'body-parser';
import logger from 'morgan';
import apiRoutes from './routes/api';

const app = express();
const port = 3000;

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
app.use('/api', apiRoutes);

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
