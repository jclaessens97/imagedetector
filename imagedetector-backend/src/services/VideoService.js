import youtubedl from 'youtube-dl';
import ffmpeg from 'fluent-ffmpeg';
import fs from 'fs';
import path from 'path';
import * as util from '../util/util.js';

export async function upload(sourceImage, videoId, frequency) {
    const videoPath = path.join('tmp', 'videos', `${videoId}.mp4`);

    // TODO: if file already exists, skip download
    const { snapshotPoints } = await download(videoId, frequency, videoPath);

    await createSnapshots(snapshotPoints, videoPath, videoId);

    const resultList = [];
    for (let i = 1; i <= snapshotPoints.length; i++) {
        const snapshotPath =  path.join('tmp', 'snapshots', `${videoId}_${i}.png`);

        //create object to be consistent with the sourceImage & imageService.
        //imageCompare() uses path property of an object to create a matrix of an image.
        const targetImage = {
          path: snapshotPath
        };

        const result = await util.imageCompare(sourceImage, targetImage);
        console.log('%s contains %b', targetImage, result);

        let resultObj = {
          timestamp: snapshotPoints[i - 1],
          containsSource: result
        };
        resultList.push(resultObj);

        util.remove(snapshotPath);
    }

    util.remove(videoPath);
    util.remove(sourceImage.path);

    return resultList;
}

async function download(videoId, frequency, videoPath) {
  const url =`http://www.youtube.com/watch?v=${videoId}`;
  const video = youtubedl(url);

  const startDownloadPromise = new Promise((resolve, reject) => {
    video.on('info', (info) => {
      video.pipe(fs.createWriteStream(videoPath));
      console.log('Download started!');

      const snapshotPoints = calculateSnapshotPoints(info.duration, frequency);
      resolve(snapshotPoints);
    });
  });

  const endDownloadPromise = new Promise((resolve, reject) => {
    video.on('end', () => {
      console.log('Download ended!');
      resolve(true);
    });
  });

  const results = await Promise.all([startDownloadPromise, endDownloadPromise]);
  return {
    snapshotPoints: results[0],
    finished: results[1],
  };
}

function calculateSnapshotPoints(duration, frequency) {
  console.log('calculateSnapshotPoints() called');

  // TODO: 'include end'-parameter

  // parse duration & convert it to seconds.
  const splittedDuration = duration.split(':');
  const mins = parseInt(splittedDuration[0]);
  const secs = parseInt(splittedDuration[1]);
  const totalSeconds = (mins * 60) + secs;

  const snapshotPoints = [];

  // Determine snapshots, format them to a string and store them to an array
  for (let i = 0; i < totalSeconds; i++) {
    if (i % frequency === 0) {
      const minutes = Math.floor(i / 60);
      const seconds = (i % 60);
      const formattedTime = `${minutes}:${seconds < 10 ? '0' + seconds : seconds}`;
      snapshotPoints.push(formattedTime);
    }
  }

  return snapshotPoints;
}

function createSnapshots(snapshotPoints, videoPath, videoId) {
  const basePath = path.join('tmp', 'snapshots');

  return new Promise((resolve, reject) => {
    ffmpeg(videoPath)
    .screenshots({
      timestamps: snapshotPoints,
      filename: `${videoId}_%i`,
      folder: basePath,
      size: '500x281'
    })
    .on('end', () => {
      console.log('snapshots created');
      resolve();
    });
  });
}
