import * as util from '../util/util.js';

export async function upload(sourceFile, targetFile) {
  console.log('ImageService.upload() image called');

  const result = await util.imageCompare(sourceFile, targetFile);
  util.remove(sourceFile);
  util.remove(targetFile);
  return result;
}
